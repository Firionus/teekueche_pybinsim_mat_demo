# teekueche_pybinsim_mat_demo

:bangbang:**Unusable for the public**:bangbang:  
This demo requires the private branch `ConvolverMultisourceMat` of
pyBinSim, so you probably cannot run it. This repository is public only
for posterity.

## Description

A demo of binaural spatial audio in a little kitchen, utilitzing the
`ConvolverMultisourceMat` branch of pyBinSim. Features going into the corridor
outside the room with door open or closed. 

## Getting Started

- Ensure you have the following software installed
    - [Git](https://git-scm.com/) and [Git LFS](https://git-lfs.github.com/)  (check with `git lfs`) 
    - [conda](https://docs.conda.io/en/latest/miniconda.html) (check with `conda`)
    - Linux: development headers for libfftw and portaudio
        - Ubuntu
            ```sh
            sudo apt-get install gcc portaudio19-dev libfftw3-dev
            ```
        - Fedora
            ```sh
            sudo dnf install gcc portaudio19-devel fftw-devel
            ```
    - [Open Stage Control](http://openstagecontrol.ammd.net/download/)
- Create conda environment
    ```sh
    conda create --name binsim python=3.9 numpy
    conda activate binsim
    ```
- Get pyBinSim and install
    ```sh
    git clone <private pybinsim repo>
    cd pybinsim
    git checkout ConvolverMultisourceMat
    pip install -e .
    ```
- Clone the demo
    ```sh
    cd ..
    git clone https://gitlab.com/Firionus/teekueche_pybinsim_mat_demo.git
    cd teekueche_pybinsim_mat_demo
    ```
- Start Open Stage Control
    ```sh
    open-stage-control -- --load open-stage-control/ui.json --custom-module open-stage-control/custom-module.js --send localhost:10003 --debug true
    ```
- Ensure the conda environment is still active and run
    ```sh
    python run_binsim.py
    ```

Click all buttons in Open Stage Control at least once and you  should start to
hear something.  
You can change the parameters of the auralization in Open Stage Control. 

## Known Issues

We experienced a crash of the Python interpreter on one system without any logging output, probably https://github.com/spatialaudio/python-sounddevice/issues/256. Try the solution suggested there:
```sh
pip uninstall sounddevice
conda install -c conda-forge python-sounddevice
```
