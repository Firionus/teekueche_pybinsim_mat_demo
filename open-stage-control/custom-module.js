// Do whatever you want
// initialize variables
// declare functions
// load modules
// etc

// 1 value to select channel, 15 values to select BRIR
// var brirId = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

const brirId = [
    { type: 'i', value: 0 }, // 0 channel
    { type: 'i', value: 0 }, // 1 listener yaw
    { type: 'i', value: 0 }, // 2 listener pitch
    { type: 'i', value: 0 }, // 3 listener roll
    { type: 'i', value: 0 }, // 4 listener x
    { type: 'i', value: 0 }, // 5 listener y
    { type: 'i', value: 0 }, // 6 listener z
    { type: 'i', value: 0 }, // 7 source yaw
    { type: 'i', value: 0 }, // 8 source pitch
    { type: 'i', value: 0 }, // 9 source roll
    { type: 'i', value: 0 }, // 10 source x
    { type: 'i', value: 0 }, // 11 source y
    { type: 'i', value: 0 }, // 12 source z
    { type: 'i', value: 1 }, // 13 custom a (measurementRound)
    { type: 'i', value: 0 }, // 14 custom b (door)
    { type: 'i', value: 0 }, // 15 custom c
]

const lateId = [
    { type: 'i', value: 0 }, // 0 channel
    { type: 'i', value: 0 }, // 1 listener yaw
    { type: 'i', value: 0 }, // 2 listener pitch
    { type: 'i', value: 0 }, // 3 listener roll
    { type: 'i', value: 0 }, // 4 listener x
    { type: 'i', value: 0 }, // 5 listener y
    { type: 'i', value: 0 }, // 6 listener z
    { type: 'i', value: 0 }, // 7 source yaw
    { type: 'i', value: 0 }, // 8 source pitch
    { type: 'i', value: 0 }, // 9 source roll
    { type: 'i', value: 0 }, // 10 source x
    { type: 'i', value: 0 }, // 11 source y
    { type: 'i', value: 0 }, // 12 source z
    { type: 'i', value: 1 }, // 13 custom a (measurementRound)
    { type: 'i', value: 0 }, // 14 custom b (door)
    { type: 'i', value: 0 }, // 15 custom c
]


// step: rounds to step, so e.g. with step=2 to -4, -2, 0, 2, 4, ...
const mapbrir = (args, from, to, step) => {
    let input = args[from].value;
    if (step) {
        input = step * Math.round(input / step);
        if (input === -0.0) {
            input = 0.;
        }
    }
    brirId[to] = input;
};

const maplate = (args, from, to) => {
    let input = args[from].value;
    lateId[to] = input;
};

const send_brir_select = () => {
    send('localhost', 10000, '/pyBinSim_ds_Filter', ...brirId);
    send('localhost', 10001, '/pyBinSim_early_Filter', ...brirId);
};

const send_late_select = () => {
    send('localhost', 10002, '/pyBinSim_late_Filter', ...lateId);
};


module.exports = {

    init: function () {
        // this will be executed once when the osc server starts
        console.log("Hello World! This is my custom module for my pyBinSim test.");
    },

    oscOutFilter: function (data) {

        var { address, args, host, port, clientId } = data

        // merge
        if (address === '/listener_orientation') {
            mapbrir(args, 0, 1, 5);
            mapbrir(args, 1, 2, 5);

            send_brir_select()

            return // bypass original message
        } else if (address === '/listenerPosition') {
            mapbrir(args, 0, 4);
            mapbrir(args, 1, 5);
            mapbrir(args, 2, 6);

            maplate(args, 0, 4);
            maplate(args, 1, 5);
            maplate(args, 2, 6);

            send_brir_select()
            send_late_select()

            return // bypass original message
        } else if (address === '/sourceSelect') {
            mapbrir(args, 0, 7);
            mapbrir(args, 1, 8);
            mapbrir(args, 2, 9);
            mapbrir(args, 3, 10);
            mapbrir(args, 4, 11);
            mapbrir(args, 5, 12);

            maplate(args, 0, 7);
            maplate(args, 1, 8);
            maplate(args, 2, 9);
            maplate(args, 3, 10);
            maplate(args, 4, 11);
            maplate(args, 5, 12);

            send_brir_select()
            send_late_select()

            return // bypass original message
        } else if (address === '/door') {
            mapbrir(args, 0, 14);

            maplate(args, 0, 14);

            send_brir_select()
            send_late_select()

            return // bypass original message
        } else if (address === '/measurementRun') {
            mapbrir(args, 0, 13);

            maplate(args, 0, 13);

            send_brir_select()
            send_late_select()

            return // bypass original message
        }

        return { address, args, host, port }
    }


}