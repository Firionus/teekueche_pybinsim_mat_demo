"""
Read a pyBinSim MAT filter file and output a JSON with all available filter

Usage example: `python generate_dense_filter_json.py
brirs/merged.matbrirs/merged_dense.json`

This script will assume that filters for DS, ER and LR each are sampled densely
in a rectangle of all encountered values for (listener_orientation,
listener_position, source (orientation + position), custom). Missing filters are
reported, the JSON file is generated regardless. 
"""
from scipy.io import loadmat
import sys
import numpy as np
import json
import itertools


def parse_mat_and_write_csv(mat_filepath, json_filepath):
    print(f"Reading MAT file {mat_filepath}")
    mat = loadmat(mat_filepath)
    ds_individual_keys = [set() for _ in range(4)]
    er_individual_keys = [set() for _ in range(4)]
    lr_individual_keys = [set() for _ in range(4)]
    ds_keys = set()
    er_keys = set()
    lr_keys = set()

    # collect filter keys per key type
    for var in (var for var in mat if not var in ('__header__', '__version__', '__globals__')):
        filters = mat[var]
        for i in range(filters.shape[1]):
            filter = filters[0,i]
            typestr = filter[0][0]
            listener_orientation = tuple(np.squeeze(filter[1]).astype(np.float64))
            listener_position = tuple(np.squeeze(filter[2]).astype(np.float64))
            source = tuple(np.concatenate((
                np.squeeze(filter[3]).astype(np.float64),
                np.squeeze(filter[4]).astype(np.float64))
                ))
            custom = tuple(np.squeeze(filter[5]).astype(np.float64))
            keylist = (
                listener_orientation, listener_position, source, custom
            )
            if typestr == 'DS':
                ds_keys.add(keylist)
                for i in range(4):
                    ds_individual_keys[i].add(keylist[i])
            elif typestr == 'ER':
                er_keys.add(keylist)
                for i in range(4):
                    er_individual_keys[i].add(keylist[i])
            elif typestr == 'LR':
                lr_keys.add(keylist)
                for i in range(4):
                    lr_individual_keys[i].add(keylist[i])
            else:
                raise Exception("type of filter was not DS, ER or LR")
    # check all combinations of keys exist
    print("checking filters for dense shape as product of listener_orientation, listener_position, source, custom")
    
    ds_expected = 0
    er_expected = 0
    lr_expected = 0
    ds_missing = 0
    er_missing = 0
    lr_missing = 0
    listener_orientation_missing = set()
    listener_position_missing = set()
    source_missing = set()
    custom_missing = set()
    for combination in itertools.product(*ds_individual_keys):
        ds_expected += 1
        if not combination in ds_keys:
            print("missing DS combination:", combination)
            ds_missing += 1
            listener_orientation_missing.add(combination[0])
            listener_position_missing.add(combination[1])
            source_missing.add(combination[2])
            custom_missing.add(combination[3])
    for combination in itertools.product(*er_individual_keys):
        er_expected += 1
        if not combination in er_keys:
            print("missing ER combination:", combination)
            er_missing += 1
            listener_orientation_missing.add(combination[0])
            listener_position_missing.add(combination[1])
            source_missing.add(combination[2])
            custom_missing.add(combination[3])
    for combination in itertools.product(*lr_individual_keys):
        lr_expected += 1
        if not combination in lr_keys:
            print("missing LR combination:", combination)
            lr_missing += 1
            listener_orientation_missing.add(combination[0])
            listener_position_missing.add(combination[1])
            source_missing.add(combination[2])
            custom_missing.add(combination[3])
    all_missing = ds_missing + er_missing + lr_missing
    all_expected = ds_expected + er_expected + lr_expected
    if all_missing != 0:
        print("### WARNING - missing filters ###")
        print(f"involved listener_orientation: {listener_orientation_missing}")
        print(f"involved listener_position: {listener_position_missing}")
        print(f"involved source: {source_missing}")
        print(f"involved custom: {custom_missing}")
        print(f"DS: expected {ds_expected} filters, {ds_missing} were missing")
        print(f"ER: expected {er_expected} filters, {er_missing} were missing")
        print(f"LR: expected {lr_expected} filters, {lr_missing} were missing")
        print(f"all: expected {all_expected} filters, {all_missing} were missing")
    output = {
        'DS': {
            'listener_orientation': list(ds_individual_keys[0]),
            'listener_position': list(ds_individual_keys[1]),
            'source': list(ds_individual_keys[2]),
            'custom': list(ds_individual_keys[3]),
        },
        'ER': {
            'listener_orientation': list(er_individual_keys[0]),
            'listener_position': list(er_individual_keys[1]),
            'source': list(er_individual_keys[2]),
            'custom': list(er_individual_keys[3]),
        },
        'LR': {
            'listener_orientation': list(lr_individual_keys[0]),
            'listener_position': list(lr_individual_keys[1]),
            'source': list(lr_individual_keys[2]),
            'custom': list(lr_individual_keys[3]),
        },
    }
    with open(json_filepath, 'w', newline='') as jsonfile:
        json.dump(output, jsonfile)




def main():
    mat_filepath = sys.argv[1]
    json_filepath = sys.argv[2]
    parse_mat_and_write_csv(mat_filepath, json_filepath)


if __name__ == '__main__':
    main()
